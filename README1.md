# Build a Kubernetes HA-cluster using k3s & kube-vip & metal-lb via Ansible

*Based on <https://docs.technotim.live/posts/k3s-etcd-ansible/> and <https://github.com/k3s-io/k3s-ansible*

BIG SHOUTOUT TO [TechnoTim](https://github.com/timothystewart6) who made this possible and inspired me be sure to check him out!


## Prerequisities

[Basic Environment](https://gitlab.com/pxmx1/sdenv/basicenv)

- dnsmasq auf Proxmox
- Einträge in /etc/hosts am Beispiel 192.168.193.0/24. Die Einträge sind entsprechend den IPs in k3s anzupassen
```
192.168.193.80 rancher.my.lan
192.168.193.80 traefik.my.lan
192.168.193.80 dashboard.my.lan
192.168.193.80 grafana.my.lan  
```

Die IP-Adressen erhält man durch 
```
kubectl get svc -A
```

- Anpassungen erforderlich in 
  - inventory/my-cluster/hosts.ini
  - inventory/my-cluster/group_vars/all.yml
  - inventory/my-cluster/group_vars/globals.yml
  - inventory/my-cluster/group_vars/freeipa.yml
  - inventory/my-cluster/group_vars/proxmox.yml

## Ansible-Aufrufe

- VMs anlegen
```
ansible-playbook site_vm.yml
```
   
- K3s installieren   
```
ansible-playbook site_k3s.yml
```

## Kubectl Aufrufe

```
kubectl get pods -A
kubectl get svc -A
kubectl get namespaces 
kubectl get ingress -A
```

## k9s dashboard

k9s wird auf k3-m1 installiert. Aufruf im Terminal mit `k9s`

## Rancher Dashboard

Bei der ersten Anmeldung muß das bootstrap Kennwort ermittelt und neu gesetzt werden.

```
kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{ .data.bootstrapPassword|base64decode}}{{ "\n" }}'
```

## traefik Dashboard

https://traefik.my.lan

## grafana Dashboard

https://grafana.my.lan

Username: admin Kennwort: password